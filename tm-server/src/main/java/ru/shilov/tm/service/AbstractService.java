package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.SneakyThrows;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.service.IService;
import ru.shilov.tm.api.service.ISettingService;
import ru.shilov.tm.entity.AbstractEntity;
import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.util.DbUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@SuperBuilder
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    protected ISettingService settingService;

    @NotNull
    public Connection getConnection(@NotNull final ISettingService settingService) {
        @NotNull final String host = settingService.findByName("db.host").getValue();
        @NotNull final String name = settingService.findByName("db.name").getValue();
        @NotNull final String port = settingService.findByName("db.port").getValue();
        @NotNull final String login = settingService.findByName("db.login").getValue();
        @NotNull final String password = settingService.findByName("db.password").getValue();
        return DbUtil.getConnection(host, name, port, login, password);
    }

    @NotNull
    @Override
    public List<T> findAll() {
        try {
            return getRepository(getConnection(settingService)).findAll();
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @NotNull
    @Override
    public T findOne(@Nullable final String id) throws RuntimeException {
        try {
            @Nullable final T entity;
            if (Strings.isNullOrEmpty(id) || (entity = getRepository(getConnection(settingService)).findOne(id)) == null) {
                throw new NoSuchEntityException();
            }
            return entity;
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @Override
    public void removeAll() {
        try {
            getRepository(getConnection(settingService)).removeAll();
        } catch (SQLException e) {
            throw new EntityRemoveException();
        }
    }

    @Nullable
    @Override
    public T persist(@Nullable final T entity) throws RuntimeException {
        try {
            if (entity == null) throw new EntityPersistException();
            return getRepository(getConnection(settingService)).persist(entity);
        } catch (SQLException e) {
            throw new EntityPersistException();
        }
    }

    @Nullable
    @Override
    public T merge(@Nullable final T entity) throws RuntimeException {
        try {
            if (entity == null) throw new EntityMergeException();
            return getRepository(getConnection(settingService)).merge(entity);
        } catch (SQLException e) {
            throw new EntityMergeException();
        }
    }

    @NotNull
    abstract IRepository<T> getRepository(@NotNull final Connection connection);

}
