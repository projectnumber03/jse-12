package ru.shilov.tm.service;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.Nulls;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.AllArgsConstructor;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.service.IDataTransportService;
import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.domain.Domain;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityPersistException;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public final class DataTrasportServiceImpl implements IDataTransportService {

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @Override
    public void saveDataBin() throws IOException {
        try (@NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("./saved/entities.bin"))) {
            objectOutputStream.writeObject(getDomain());
        }
    }

    @Override
    public void loadDataBin() throws Exception {
        removeAll();
        try (@NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("./saved/entities.bin"))) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            persistAll(domain);
        }
    }

    @Override
    public void saveDataXmlByJaxB() throws JAXBException {
        @NotNull final Marshaller jaxbMarshaller = JAXBContextFactory.createContext(new Class[]{Domain.class}, null).createMarshaller();
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/xml");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(getDomain(), new File("./saved/entities.xml"));
    }

    @Override
    public void loadDataXmlByJaxB() throws Exception {
        removeAll();
        @NotNull final Unmarshaller unmarshaller = JAXBContextFactory.createContext(new Class[]{Domain.class}, null).createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/xml");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(new File("./saved/entities.xml"));
        persistAll(domain);
    }

    @Override
    public void saveDataJsonByJaxB() throws Exception {
        @NotNull final Marshaller jaxbMarshaller = JAXBContextFactory.createContext(new Class[]{Domain.class}, Collections.emptyMap()).createMarshaller();
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(getDomain(), new File("./saved/entities.json"));
    }

    @Override
    public void loadDataJsonByJaxB() throws Exception {
        removeAll();
        @NotNull final Unmarshaller unmarshaller = JAXBContextFactory.createContext(new Class[]{Domain.class}, null).createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(new File("./saved/entities.json"));
        persistAll(domain);
    }

    @Override
    public void saveDataXmlByFasterXml() throws IOException {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(new File("./saved/entities.xml"), getDomain());
    }

    @Override
    public void loadDataXmlByFasterXml() throws Exception {
        removeAll();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.configOverride(List.class).setSetterInfo(JsonSetter.Value.forValueNulls(Nulls.AS_EMPTY));
        @NotNull final Domain domain = xmlMapper.readValue(new File("./saved/entities.xml"), Domain.class);
        persistAll(domain);
    }

    @Override
    public void saveDataJsonByFasterXml() throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File("./saved/entities.json"), getDomain());
    }

    @Override
    public void loadDataJsonByFasterXml() throws Exception {
        removeAll();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        @NotNull final Domain domain = objectMapper.readValue(new File("./saved/entities.json"), Domain.class);
        persistAll(domain);
    }

    private Domain getDomain() {
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final List<User> users = userService.findAll();
        return new Domain(projects, tasks, users);
    }

    private void removeAll() {
        projectService.removeAll();
        taskService.removeAll();
        userService.removeAll();
    }

    private void persistAll(@NotNull final Domain domain) throws Exception {
        for (@NotNull final Project p : domain.getProjects()) {
            projectService.persist(p);
        }
        for (@NotNull final Task t : domain.getTasks()) {
            taskService.persist(t);
        }
        for (@NotNull final User u : domain.getUsers()) {
            userService.persist(u);
        }
    }

}
