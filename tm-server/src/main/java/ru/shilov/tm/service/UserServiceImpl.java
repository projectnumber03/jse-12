package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.repository.ISettingRepository;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.api.service.ISettingService;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;
import ru.shilov.tm.repository.UserRepositoryImpl;
import ru.shilov.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;

@SuperBuilder
public final class UserServiceImpl extends AbstractService<User> implements IUserService {

    @NotNull
    @Override
    public Boolean removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        try {
            if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
            return getRepository(getConnection(settingService)).removeOneByUserId(userId, id);
        } catch (SQLException e) {
            throw new EntityRemoveException();
        }
    }

    @NotNull
    @Override
    public User register(@Nullable final User user) {
        try {
            if (user == null) throw new EntityPersistException();
            @NotNull final String salt = settingService.findByName("salt").getValue();
            @NotNull final String cycle = settingService.findByName("cycle").getValue();
            user.setPassword(SignatureUtil.sign(user.getPassword(), salt, Integer.parseInt(cycle)));
            return getRepository(getConnection(settingService)).persist(user);
        } catch (SQLException e) {
            throw new EntityPersistException();
        }
    }

    @Nullable
    @Override
    public User merge(@Nullable final User user) throws RuntimeException {
        if (user == null) throw new EntityMergeException();
        @NotNull final String salt = settingService.findByName("salt").getValue();
        @NotNull final String cycle = settingService.findByName("cycle").getValue();
        user.setPassword(SignatureUtil.sign(user.getPassword(), salt, Integer.parseInt(cycle)));
        return super.merge(user);
    }

    @NotNull
    @Override
    public String getId(@Nullable final String value) throws NumberToIdTransformException {
        try {
            if (Strings.isNullOrEmpty(value) || !isNumber(value)) throw new NumberToIdTransformException(value);
            return getRepository(getConnection(settingService)).getId(Integer.parseInt(value) - 1);
        } catch (SQLException e) {
            throw new NumberToIdTransformException(value);
        }
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws NoSuchEntityException {
        try {
            @Nullable final User user;
            if (Strings.isNullOrEmpty(login) || ((user = getRepository(getConnection(settingService)).findByLogin(login)) == null)){
                throw new NoSuchEntityException();
            }
            return user;
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    private boolean isNumber(@NotNull final String value) {
        return value.matches("\\d+") && Integer.parseInt(value) > 0;
    }

    @Override
    @NotNull
    public UserRepositoryImpl getRepository(@NotNull final Connection connection) {
        return UserRepositoryImpl.builder().connection(connection).build();
    }

}
