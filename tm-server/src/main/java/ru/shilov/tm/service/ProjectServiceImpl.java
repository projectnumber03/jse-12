package ru.shilov.tm.service;

import com.google.common.base.Strings;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;
import ru.shilov.tm.repository.ProjectRepositoryImpl;
import ru.shilov.tm.repository.TaskRepositoryImpl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

@SuperBuilder
public final class ProjectServiceImpl extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Override
    public List<Project> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        try {
            if (Strings.isNullOrEmpty(userId)) throw new NoSuchEntityException();
            return getRepository(getConnection(settingService)).findByUserId(userId);
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDescription(@Nullable String userId, @Nullable String value) throws NoSuchEntityException {
        try {
            if (Strings.isNullOrEmpty(userId) || Strings.isNullOrEmpty(value)) throw new NoSuchEntityException();
            return getRepository(getConnection(settingService)).findByNameOrDescription(userId, value);
        } catch (SQLException e) {
            throw new NoSuchEntityException();
        }
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        try {
            if (Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
            return getRepository(getConnection(settingService)).removeByUserId(userId);
        } catch (SQLException e) {
            throw new EntityRemoveException();
        }
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException {
        try {
            if (Strings.isNullOrEmpty(id) || Strings.isNullOrEmpty(userId)) throw new EntityRemoveException();
            @NotNull final List<Task> projectTasks = getTaskRepository(getConnection(settingService)).findByProjectId(userId, id);
            for (@NotNull final String taskId : projectTasks.stream().map(Task::getId).collect(Collectors.toList())) {
                getTaskRepository(getConnection(settingService)).removeOneByUserId(userId, taskId);
            }
            getRepository(getConnection(settingService)).removeOneByUserId(userId, id);
            return true;
        } catch (SQLException e) {
            throw new EntityRemoveException();
        }
    }

    @NotNull
    @Override
    public String getId(@Nullable final String userId, @Nullable final String value) throws NumberToIdTransformException {
        try {
            if (Strings.isNullOrEmpty(value) || !isNumber(value) || Strings.isNullOrEmpty(userId)) throw new NumberToIdTransformException(value);
            return getRepository(getConnection(settingService)).getId(userId, Integer.parseInt(value) - 1);
        } catch (SQLException e) {
            throw new NumberToIdTransformException(value);
        }
    }

    private boolean isNumber(@NotNull final String value) {
        return value.matches("\\d+") && Integer.parseInt(value) > 0;
    }

    @Override
    @NotNull
    public ProjectRepositoryImpl getRepository(@NotNull final Connection connection) {
        return ProjectRepositoryImpl.builder().connection(connection).build();
    }

    @NotNull
    private TaskRepositoryImpl getTaskRepository(@NotNull final Connection connection) {
        return TaskRepositoryImpl.builder().connection(connection).build();
    }

}
