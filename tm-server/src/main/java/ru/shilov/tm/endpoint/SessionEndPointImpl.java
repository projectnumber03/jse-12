package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.ISessionEndPoint;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.ISettingService;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.Setting;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.ISessionEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class SessionEndPointImpl implements ISessionEndPoint {

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    @NotNull
    private ISettingService settingService;

    @Nullable
    @Override
    @WebMethod
    public Session createSession(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable final User user = userService.findByLogin(login);
        @Nullable final String salt = settingService.findByName("salt").getValue();
        @Nullable final Setting cycleSetting = settingService.findByName("cycle");
        @Nullable final Integer cycle = Integer.parseInt(cycleSetting.getValue());
        if(user.getPassword() != null && !user.getPassword().equals(SignatureUtil.sign(password, salt, cycle))) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session, salt, cycle));
        sessionService.persist(session);
        return session;
    }

    @Override
    @WebMethod
    public void removeSession(@Nullable final String userId, @Nullable final String id) throws RuntimeException {
        sessionService.removeOneByUserId(userId, id);
    }

}
