package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.IProjectEndPoint;
import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.IProjectEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectEndPointImpl implements IProjectEndPoint {

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjects(@Nullable final Session session) {
        sessionService.validateSession(session);
        return projectService.findAll();
    }

    @NotNull
    @Override
    @WebMethod
    public Project findOneProject(@Nullable final Session session, @NotNull final String id) throws Exception {
        sessionService.validateSession(session);
        return projectService.findOne(id);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findProjectsByUserId(@Nullable final Session session) {
        sessionService.validateSession(session);
        return projectService.findByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(@Nullable final Session session) {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        projectService.removeAll();
    }

    @NotNull
    @Override
    public Boolean removeProjectsByUserId(@Nullable final Session session) {
        sessionService.validateSession(session);
        return projectService.removeByUserId(session.getUserId());
    }

    @NotNull
    @Override
    public Boolean removeOneProjectByUserId(@Nullable final Session session, @Nullable final String id) throws Exception {
        sessionService.validateSession(session);
        return projectService.removeOneByUserId(session.getUserId(), id);
    }

    @Nullable
    @Override
    public Project persistProject(@Nullable final Session session, @Nullable final Project p) throws Exception {
        sessionService.validateSession(session);
        return projectService.persist(p);
    }

    @Nullable
    @Override
    public Project mergeProject(@Nullable final Session session, @NotNull final Project p) throws Exception {
        sessionService.validateSession(session);
        return projectService.merge(p);
    }

    @NotNull
    @Override
    public String getProjectId(@Nullable final Session session, @NotNull final String value) throws Exception {
        sessionService.validateSession(session);
        return projectService.getId(session.getUserId(), value);
    }

    @NotNull
    @Override
    public List<Project> findProjectsByNameOrDescription(@Nullable final Session session, @NotNull final String value) throws Exception {
        sessionService.validateSession(session);
        return projectService.findByNameOrDescription(session.getUserId(), value);
    }

}
