package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.IDataTransportEndPoint;
import ru.shilov.tm.api.service.IDataTransportService;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import javax.jws.WebService;
import java.util.Arrays;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.IDataTransportEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class DataTransportEndPointImpl implements IDataTransportEndPoint {

    @NotNull
    private IDataTransportService dataTransportService;

    @NotNull
    private ISessionService sessionService;

    @Override
    public void saveDataBin(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.saveDataBin();
    }

    @Override
    public void loadDataBin(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.loadDataBin();
    }

    @Override
    public void saveDataXmlByJaxB(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.saveDataXmlByJaxB();
    }

    @Override
    public void loadDataXmlByJaxB(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.loadDataXmlByJaxB();
    }

    @Override
    public void saveDataJsonByJaxB(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.saveDataJsonByJaxB();
    }

    @Override
    public void loadDataJsonByJaxB(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.loadDataJsonByJaxB();
    }

    @Override
    public void saveDataXmlByFasterXml(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.saveDataXmlByFasterXml();
    }

    @Override
    public void loadDataXmlByFasterXml(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.loadDataXmlByFasterXml();
    }

    @Override
    public void saveDataJsonByFasterXml(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.saveDataJsonByFasterXml();
    }

    @Override
    public void loadDataJsonByFasterXml(@Nullable final Session session) throws Exception {
        sessionService.validateSession(session, Arrays.asList(User.Role.ADMIN));
        dataTransportService.loadDataJsonByFasterXml();
    }
}
