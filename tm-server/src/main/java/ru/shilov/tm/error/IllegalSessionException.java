package ru.shilov.tm.error;

public final class IllegalSessionException extends RuntimeException {

    public IllegalSessionException() {
        super("Сессия недействительна");
    }

}
