package ru.shilov.tm.error;

public final class SessionTimeOutException extends RuntimeException {

    public SessionTimeOutException() {
        super("Сессия устарела, войдите в систему повторно");
    }
}
