package ru.shilov.tm.error;

public final class NoSuchSessionException extends RuntimeException {

    public NoSuchSessionException() {
        super("Сессия не найдена");
    }

}
