package ru.shilov.tm.error;

public final class NoSuchEntityException extends RuntimeException {

    public NoSuchEntityException() {
        super("Ошибка получения объекта");
    }

}
