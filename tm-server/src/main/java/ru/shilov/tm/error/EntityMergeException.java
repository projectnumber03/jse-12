package ru.shilov.tm.error;

public final class EntityMergeException extends RuntimeException {

    public EntityMergeException() {
        super("Ошибка обновления объекта");
    }

}
