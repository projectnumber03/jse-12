package ru.shilov.tm.error;

public final class NumberToIdTransformException extends RuntimeException {

    public NumberToIdTransformException(final String value) {
        super(String.format("Ошибка получения id %s", value));
    }

}
