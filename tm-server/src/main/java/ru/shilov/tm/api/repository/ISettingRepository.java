package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Setting;

public interface ISettingRepository {

    @Nullable
    Setting findByName(@NotNull final String name);

}
