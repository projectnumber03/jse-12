package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Project;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @NotNull
    String FIND_ALL_QUERY = "SELECT * FROM app_project";

    @NotNull
    String FIND_ONE_QUERY = "SELECT * FROM app_project WHERE id = ?";

    @NotNull
    String REMOVE_ALL_QUERY = "DELETE FROM app_project WHERE id IN (SELECT id FROM app_project)";

    @NotNull
    String REMOVE_BY_USER_ID_QUERY = "DELETE FROM app_project WHERE user_id = ?";

    @NotNull
    String REMOVE_ONE_BY_USER_ID_QUERY = "DELETE FROM app_project WHERE user_id = ? AND id = ?";

    @NotNull
    String PERSIST_QUERY = "INSERT INTO app_project VALUES (?, ?, ?, ?, ?, ?, ?)";

    @NotNull
    String MERGE_QUERY = "UPDATE app_project SET name = ?, description = ?, dateBegin = ?, dateEnd = ?, status = ? WHERE id = ?";

    @NotNull
    String FIND_BY_USER_ID_QUERY = "SELECT * FROM app_project WHERE user_id = ?";

    @NotNull
    String FIND_BY_NAME_DESCRIPTION_QUERY = "SELECT * FROM app_project WHERE user_id = ? AND (description LIKE ? OR name LIKE ?)";

    @NotNull
    String GET_ID_QUERY = "SELECT id FROM app_project WHERE user_id = ? LIMIT 1 OFFSET ?";

    @NotNull
    String getId(@NotNull final String userId, @NotNull final Integer value) throws SQLException;

    @NotNull
    List<Project> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) throws SQLException;

    @NotNull
    Boolean removeByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    List<Project> findByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @Nullable
    Project fetch(@Nullable final ResultSet row) throws SQLException;

}
