package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll();

    @NotNull
    T findOne(@Nullable final String id) throws Exception;

    void removeAll();

    @Nullable
    T persist(@Nullable final T entity) throws Exception;

    @Nullable
    T merge(@Nullable final T entity) throws Exception;

}
