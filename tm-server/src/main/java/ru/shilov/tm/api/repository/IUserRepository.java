package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    String REMOVE_ONE_BY_USER_ID_QUERY = "DELETE FROM app_user WHERE id = ? AND id != ?";

    @NotNull
    String FIND_BY_LOGIN_QUERY = "SELECT * FROM app_user WHERE login = ?";

    @NotNull
    String GET_ID_QUERY = "SELECT id FROM app_user LIMIT 1 OFFSET ?";

    @NotNull
    String FIND_ALL_QUERY = "SELECT * FROM app_user";

    @NotNull
    String FIND_ONE_QUERY = "SELECT * FROM app_user WHERE id = ?";

    @NotNull
    String REMOVE_ALL_QUERY = "DELETE FROM app_user WHERE id IN (SELECT id FROM app_user)";

    @NotNull
    String PERSIST_QUERY = "INSERT INTO app_user VALUES (?, ?, ?, ?)";

    @NotNull
    String MERGE_QUERY = "UPDATE app_user SET login = ?, password = ?, role = ? WHERE id = ?";

    @NotNull
    String getId(@NotNull final Integer value) throws SQLException;

    @NotNull
    Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @Nullable
    User findByLogin(@NotNull final String login) throws SQLException;

    @Nullable
    User fetch(@Nullable final ResultSet row) throws SQLException;

}
