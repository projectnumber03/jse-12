package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    String FIND_ONE_BY_USER_ID_QUERY = "SELECT * FROM app_session WHERE user_id = ? AND id = ?";

    @NotNull
    String REMOVE_ONE_BY_USER_ID_QUERY = "DELETE FROM app_session WHERE user_id = ? AND id = ?";

    @NotNull
    String FIND_ALL_QUERY = "SELECT * FROM app_session";

    @NotNull
    String FIND_ONE_QUERY = "SELECT * FROM app_session WHERE id = ?";

    @NotNull
    String REMOVE_ALL_QUERY = "DELETE FROM app_session WHERE id IN (SELECT id FROM app_session)";

    @NotNull
    String PERSIST_QUERY = "INSERT INTO app_session VALUES(?, ?, ?, ?)";

    @Nullable
    Session findOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @NotNull
    Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

}
