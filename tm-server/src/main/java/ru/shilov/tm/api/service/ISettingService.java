package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.Setting;

public interface ISettingService {

    @NotNull
    Setting findByName(@NotNull final String name);

}
