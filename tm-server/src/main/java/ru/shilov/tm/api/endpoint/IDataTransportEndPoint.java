package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@WebService
public interface IDataTransportEndPoint {

    @WebMethod
    void saveDataBin(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataBin(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataXmlByJaxB(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataXmlByJaxB(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataJsonByJaxB(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataJsonByJaxB(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataXmlByFasterXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataXmlByFasterXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void saveDataJsonByFasterXml(@Nullable final Session session) throws Exception;

    @WebMethod
    void loadDataJsonByFasterXml(@Nullable final Session session) throws Exception;

}
