package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Task;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    String FIND_BY_PROJECT_ID_QUERY = "SELECT * FROM app_task where user_id = ? AND project_id = ?";

    @NotNull
    String FIND_BY_NAME_DESCRIPTION_QUERY = "SELECT * FROM app_task WHERE user_id = ? AND (description LIKE ? OR name LIKE ?)";

    @NotNull
    String GET_ID_QUERY = "SELECT id FROM app_task WHERE user_id = ? LIMIT 1 OFFSET ?";

    @NotNull
    String FIND_BY_USER_ID_QUERY = "SELECT * FROM app_task WHERE user_id = ?";

    @NotNull
    String REMOVE_BY_USER_ID_QUERY = "DELETE FROM app_task WHERE user_id = ?";

    @NotNull
    String REMOVE_ONE_BY_USER_ID_QUERY = "DELETE FROM app_task WHERE user_id = ? AND id = ?";

    @NotNull
    String FIND_ALL_QUERY = "SELECT * FROM app_task";

    @NotNull
    String FIND_ONE_QUERY = "SELECT * FROM app_task WHERE id = ?";

    @NotNull
    String REMOVE_ALL_QUERY = "DELETE FROM app_task WHERE id IN (SELECT id FROM app_task)";

    @NotNull
    String PERSIST_QUERY = "INSERT INTO app_task VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

    @NotNull
    String MERGE_QUERY = "UPDATE app_task SET name = ?, description = ?, project_id = ?, dateBegin = ?, dateEnd = ?, status = ? WHERE id = ?";

    @NotNull
    List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

    @NotNull
    List<Task> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) throws SQLException;

    @NotNull
    String getId(@NotNull final String userId, @NotNull final Integer value) throws SQLException;

    @NotNull
    Boolean removeByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    List<Task> findByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @Nullable
    Task fetch(@Nullable final ResultSet row) throws SQLException;

}
