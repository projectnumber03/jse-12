package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndPoint {

    @WebMethod
    @Nullable
    Session createSession(@NotNull final String login, @NotNull final String password) throws Exception;

    @WebMethod
    void removeSession(@Nullable final String userId, @Nullable final String id) throws EntityRemoveException;

}
