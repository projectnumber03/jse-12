package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @NotNull
    List<Project> findByUserId(@Nullable final String userId) throws NoSuchEntityException;

    @NotNull
    List<Project> findByNameOrDescription(@Nullable final String userId, @Nullable final String value) throws NoSuchEntityException;

    @NotNull
    Boolean removeByUserId(@Nullable final String userId) throws EntityRemoveException;

    @NotNull
    String getId(@Nullable final String userId, @Nullable final String value) throws NumberToIdTransformException;

    @NotNull
    Boolean removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception;

}
