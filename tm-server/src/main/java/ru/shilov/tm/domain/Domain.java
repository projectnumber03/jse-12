package ru.shilov.tm.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@JsonRootName(value = "domain")
@XmlRootElement(name = "domain")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Domain implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlProperty(localName = "project")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlProperty(localName = "task")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlProperty(localName = "user")
    @JacksonXmlElementWrapper(localName = "users")
    private List<User> users = new ArrayList<>();

}
