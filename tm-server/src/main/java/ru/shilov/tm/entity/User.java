package ru.shilov.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class User extends AbstractEntity {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private Role role;

    @Getter
    @AllArgsConstructor
    public enum Role {

        USER("Пользователь"),
        ADMIN("Администратор");

        private final String description;

    }

}
