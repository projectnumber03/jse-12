package ru.shilov.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@AllArgsConstructor
public final class Setting extends AbstractEntity {

    @Nullable
    private String name;

    @NotNull
    private String value;

}
