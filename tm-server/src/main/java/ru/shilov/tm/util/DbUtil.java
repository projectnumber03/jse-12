package ru.shilov.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.DriverManager;

public final class DbUtil {

    @SneakyThrows
    public static Connection getConnection(@NotNull final String host,
                                           @NotNull final String name,
                                           @NotNull final String port,
                                           @NotNull final String login,
                                           @NotNull final String password) {
        return DriverManager.getConnection(String.format("jdbc:mariadb://%s:%s/%s?user=%s&password=%s", host, port, name, login, password));
    }

}
