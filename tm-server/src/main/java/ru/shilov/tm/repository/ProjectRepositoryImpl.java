package ru.shilov.tm.repository;

import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.enumerated.Status;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SuperBuilder
public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public String getId(@NotNull final String userId, @NotNull final Integer value) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(GET_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setInt(2, value);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @NotNull final String projectId = resultSet.getString("id");
                return projectId;
            }
        }
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_BY_NAME_DESCRIPTION_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, String.format("%%%s%%", value));
            preparedStatement.setString(3, String.format("%%%s%%", value));
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()){
                @NotNull final List<Project> projects = new ArrayList<>();
                while (resultSet.next()) projects.add(fetch(resultSet));
                return projects;
            }
        }
    }

    @NotNull
    @Override
    public List<Project> findByUserId(@NotNull final String userId) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                @NotNull final List<Project> projects = new ArrayList<>();
                while (resultSet.next()) projects.add(fetch(resultSet));
                return projects;
            }
        }
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@NotNull final String userId) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            return preparedStatement.execute();
        }
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_ONE_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, id);
            return preparedStatement.execute();
        }
    }

    @Override
    public @NotNull List<Project> findAll() throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ALL_QUERY);
             @NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
            @NotNull final List<Project> projects = new ArrayList<>();
            while (resultSet.next()) projects.add(fetch(resultSet));
            return projects;
        }
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ONE_QUERY)) {
            preparedStatement.setString(1, id);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @Nullable final Project project = fetch(resultSet);
                return project;
            }
        }
    }

    @Override
    public void removeAll() throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_ALL_QUERY)) {
            preparedStatement.execute();
        }
    }

    @Nullable
    @Override
    public Project persist(@NotNull final Project entity) throws SQLException {
        if (entity.getStart() == null || entity.getFinish() == null) return null;
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(PERSIST_QUERY)) {
            preparedStatement.setString(1, entity.getId());
            preparedStatement.setString(2, entity.getUserId());
            preparedStatement.setString(3, entity.getName());
            preparedStatement.setString(4, entity.getDescription());
            preparedStatement.setString(5, String.valueOf(entity.getStatus()));
            preparedStatement.setDate(6, Date.valueOf(entity.getStart()));
            preparedStatement.setDate(7, Date.valueOf(entity.getFinish()));
            preparedStatement.executeUpdate();
        }
        return entity;
    }

    @Nullable
    @Override
    public Project merge(@NotNull final Project entity) throws SQLException {
        if (entity.getStart() == null || entity.getFinish() == null) return null;
        try (@NotNull final PreparedStatement statement = getConnection().prepareStatement(MERGE_QUERY)) {
            statement.setString(1, entity.getName());
            statement.setString(2, entity.getDescription());
            statement.setDate(3, Date.valueOf(entity.getStart()));
            statement.setDate(4, Date.valueOf(entity.getFinish()));
            statement.setString(5, entity.getStatus().toString());
            statement.setString(6, entity.getId());
            statement.execute();
        }
        return entity;
    }

    @Override
    public @Nullable Project fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.valueOf(row.getString("status")));
        project.setStart(row.getDate("dateBegin").toLocalDate());
        project.setFinish(row.getDate("dateEnd").toLocalDate());
        return project;
    }

}
