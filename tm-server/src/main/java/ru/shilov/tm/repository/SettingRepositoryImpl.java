package ru.shilov.tm.repository;

import com.google.common.io.Resources;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ISettingRepository;
import ru.shilov.tm.entity.AbstractEntity;
import ru.shilov.tm.entity.Setting;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

public final class SettingRepositoryImpl implements ISettingRepository {

    @NotNull
    private final Map<String, Setting> entities = new LinkedHashMap<>();

    @SneakyThrows
    public SettingRepositoryImpl() {
        @NotNull final Properties prop = new Properties();
        prop.load(Resources.getResource("application.properties").openStream());
        entities.putAll(prop.stringPropertyNames().stream().map(p -> new Setting(p, prop.getProperty(p))).collect(Collectors.toMap(AbstractEntity::getId, s -> s)));
    }

    @Nullable
    @Override
    public Setting findByName(@NotNull final String name) {
        return entities.values().stream().filter(s -> name.equals(s.getName())).findAny().orElse(null);
    }

}
