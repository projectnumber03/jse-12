package ru.shilov.tm.repository;

import lombok.SneakyThrows;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ISessionRepository;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.error.PermissionException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuperBuilder
public final class SessionRepositoryImpl extends AbstractRepository<Session> implements ISessionRepository {

    @Nullable
    @Override
    public Session findOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ONE_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, id);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @Nullable final Session session = fetch(resultSet);
                return session;
            }
        }
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_ONE_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, id);
            return preparedStatement.execute();
        }
    }

    @Override
    public @NotNull List<Session> findAll() throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ALL_QUERY);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
            @NotNull final List<Session> sessions = new ArrayList<>();
            while (resultSet.next()) sessions.add(fetch(resultSet));
            return sessions;
        }
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ONE_QUERY)) {
            preparedStatement.setString(1, id);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @Nullable final Session session = fetch(resultSet);
                return session;
            }
        }
    }

    @Override
    public void removeAll() throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_ALL_QUERY)) {
            preparedStatement.execute();
        }
    }

    @NotNull
    @Override
    public Session persist(@NotNull final Session entity) throws SQLException {
        try (@NotNull final PreparedStatement statement = getConnection().prepareStatement(PERSIST_QUERY)) {
            statement.setString(1, entity.getId());
            statement.setString(2, entity.getSignature());
            statement.setTimestamp(3, Timestamp.valueOf(entity.getCreationDate()));
            statement.setString(4, entity.getUserId());
            statement.execute();
        }
        return entity;
    }

    @Nullable
    @Override
    public Session merge(@NotNull final Session entity) {
        throw new PermissionException();
    }

    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setSignature(row.getString("signature"));
        session.setCreationDate(row.getTimestamp("timestamp").toLocalDateTime());
        session.setUserId(row.getString("user_id"));
        return session;
    }

}
