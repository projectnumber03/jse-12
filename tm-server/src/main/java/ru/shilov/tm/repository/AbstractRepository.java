package ru.shilov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.entity.AbstractEntity;

import java.sql.Connection;

@SuperBuilder
@NoArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T>, AutoCloseable {

    @Getter
    @NotNull
    private Connection connection;

    @Override
    public void close() throws Exception {
        connection.close();
    }

}
