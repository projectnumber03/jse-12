package ru.shilov.tm.repository;

import lombok.SneakyThrows;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SuperBuilder
public final class UserRepositoryImpl extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_ONE_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, id);
            preparedStatement.setString(2, userId);
            return preparedStatement.execute();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_BY_LOGIN_QUERY)) {
            preparedStatement.setString(1, login);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @Nullable final User user = fetch(resultSet);
                return user;
            }
        }
    }

    @NotNull
    @Override
    public String getId(@NotNull final Integer value) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(GET_ID_QUERY)) {
            preparedStatement.setInt(1, value);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @NotNull final String projectId = resultSet.getString("id");
                return projectId;
            }
        }
    }

    @NotNull
    @Override
    public List<User> findAll() throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ALL_QUERY);
             @NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
            @NotNull final List<User> users = new ArrayList<>();
            while (resultSet.next()) users.add(fetch(resultSet));
            return users;
        }
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ONE_QUERY)) {
            preparedStatement.setString(1, id);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @Nullable final User user = fetch(resultSet);
                return user;
            }
        }
    }

    @Override
    public void removeAll() throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_ALL_QUERY)) {
            preparedStatement.execute();
        }
    }

    @NotNull
    @Override
    public User persist(@NotNull final User entity) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(PERSIST_QUERY)) {
            preparedStatement.setString(1, entity.getId());
            preparedStatement.setString(2, entity.getLogin());
            preparedStatement.setString(3, entity.getPassword());
            preparedStatement.setString(4, String.valueOf(entity.getRole()));
            preparedStatement.executeUpdate();
        }
        return entity;
    }

    @NotNull
    @Override
    public User merge(@NotNull final User entity) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(MERGE_QUERY)) {
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setString(3, String.valueOf(entity.getRole()));
            preparedStatement.setString(4, entity.getId());
            preparedStatement.execute();
        }
        return entity;
    }

    @Nullable
    @Override
    public User fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setLogin(row.getString("login"));
        user.setPassword(row.getString("passwordHash"));
        user.setRole(User.Role.valueOf(row.getString("role")));
        return user;
    }

}
