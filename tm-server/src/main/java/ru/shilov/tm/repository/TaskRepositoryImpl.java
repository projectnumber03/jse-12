package ru.shilov.tm.repository;

import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.enumerated.Status;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SuperBuilder
public final class TaskRepositoryImpl extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_BY_PROJECT_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, projectId);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                @NotNull final List<Task> tasks = new ArrayList<>();
                while (resultSet.next()) tasks.add(fetch(resultSet));
                return tasks;
            }
        }
    }

    @NotNull
    @Override
    public List<Task> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_BY_NAME_DESCRIPTION_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, String.format("%%%s%%", value));
            preparedStatement.setString(3, String.format("%%%s%%", value));
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                @NotNull final List<Task> tasks = new ArrayList<>();
                while (resultSet.next()) tasks.add(fetch(resultSet));
                return tasks;
            }
        }
    }

    @NotNull
    @Override
    public String getId(@NotNull final String userId, @NotNull final Integer value) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(GET_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setInt(2, value);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @NotNull final String taskId = resultSet.getString("id");
                return taskId;
            }
        }
    }

    @NotNull
    @Override
    public List<Task> findByUserId(@NotNull final String userId) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                @NotNull final List<Task> tasks = new ArrayList<>();
                while (resultSet.next()) tasks.add(fetch(resultSet));
                return tasks;
            }
        }
    }

    @NotNull
    @Override
    public Boolean removeByUserId(@NotNull final String userId) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            return preparedStatement.execute();
        }
    }

    @NotNull
    @Override
    public Boolean removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_ONE_BY_USER_ID_QUERY)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, id);
            return preparedStatement.execute();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ALL_QUERY);
             @NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
            @NotNull final List<Task> tasks = new ArrayList<>();
            while (resultSet.next()) tasks.add(fetch(resultSet));
            return tasks;
        }
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String id) throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(FIND_ONE_QUERY)) {
            preparedStatement.setString(1, id);
            try (@NotNull final ResultSet resultSet = preparedStatement.executeQuery()) {
                resultSet.first();
                @Nullable final Task task = fetch(resultSet);
                return task;
            }
        }
    }

    @Override
    public void removeAll() throws SQLException {
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(REMOVE_ALL_QUERY)) {
            preparedStatement.execute();
        }
    }

    @Nullable
    @Override
    public Task persist(@NotNull final Task entity) throws SQLException {
        if (entity.getStart() == null || entity.getFinish() == null) return null;
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(PERSIST_QUERY)) {
            preparedStatement.setString(1, entity.getId());
            preparedStatement.setString(2, entity.getUserId());
            preparedStatement.setString(3, entity.getProjectId());
            preparedStatement.setString(4, entity.getName());
            preparedStatement.setString(5, entity.getDescription());
            preparedStatement.setString(6, String.valueOf(entity.getStatus()));
            preparedStatement.setDate(7, Date.valueOf(entity.getStart()));
            preparedStatement.setDate(8, Date.valueOf(entity.getFinish()));
            preparedStatement.executeUpdate();
        }
        return entity;
    }

    @Nullable
    @Override
    public Task merge(@NotNull final Task entity) throws SQLException {
        if (entity.getStart() == null || entity.getFinish() == null) return null;
        try (@NotNull final PreparedStatement preparedStatement = getConnection().prepareStatement(MERGE_QUERY)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getDescription());
            preparedStatement.setString(3, entity.getProjectId());
            preparedStatement.setDate(4, Date.valueOf(entity.getStart()));
            preparedStatement.setDate(5, Date.valueOf(entity.getFinish()));
            preparedStatement.setString(6, entity.getStatus().toString());
            preparedStatement.setString(7, entity.getId());
            preparedStatement.execute();
        }
        return entity;
    }

    @Nullable
    @Override
    public Task fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(Status.valueOf(row.getString("status")));
        task.setStart(row.getDate("dateBegin").toLocalDate());
        task.setFinish(row.getDate("dateEnd").toLocalDate());
        return task;
    }

}
