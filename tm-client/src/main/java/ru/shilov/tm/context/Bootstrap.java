package ru.shilov.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.shilov.tm.api.context.IEndPointLocator;
import ru.shilov.tm.api.context.IServiceLocator;
import ru.shilov.tm.api.context.ISessionLocator;
import ru.shilov.tm.api.endpoint.*;
import ru.shilov.tm.api.service.ITerminalService;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.command.other.HelpCommand;
import ru.shilov.tm.endpoint.*;
import ru.shilov.tm.error.IllegalCommandException;
import ru.shilov.tm.service.TerminalServiceImpl;

import java.lang.Exception;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter
public class Bootstrap implements IEndPointLocator, ISessionLocator, IServiceLocator {

    @NotNull
    private final ISessionEndPoint sessionEndPoint = new SessionEndPointImplService().getSessionEndPointImplPort();

    @NotNull
    private final IProjectEndPoint projectEndPoint = new ProjectEndPointImplService().getProjectEndPointImplPort();

    @NotNull
    private final ITaskEndPoint taskEndPoint = new TaskEndPointImplService().getTaskEndPointImplPort();

    @NotNull
    private final IUserEndPoint userEndPoint = new UserEndPointImplService().getUserEndPointImplPort();

    @NotNull
    private final IDataTransportEndPoint dataTransportEndPoint = new DataTransportEndPointImplService().getDataTransportEndPointImplPort();

    @NotNull
    private final ITerminalService terminalService = new TerminalServiceImpl();

    @Setter
    @Nullable
    private Session session;

    public void init() {
        @NotNull final Map<String, AbstractTerminalCommand> commands = initCommands();
        System.out.println("*** ДОБРО ПОЖАЛОВАТЬ В ПЛАНИРОВЩИК ЗАДАЧ ***");
        while (true) {
            try {
                @NotNull final String commandName = terminalService.nextLine();
                if (!commands.containsKey(commandName)) throw new IllegalCommandException();
                commands.get(commandName).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, AbstractTerminalCommand> initCommands() {
        @NotNull final Map<String, AbstractTerminalCommand> commands = new HashMap<>();
        @NotNull final String prefix = Arrays.stream(Package.getPackages())
                .map(Package::getName)
                .filter(name -> name.endsWith("command"))
                .findAny().orElse("");
        commands.putAll(new Reflections(prefix).getSubTypesOf(AbstractTerminalCommand.class).stream()
                .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers())).map(clazz -> {
                    try {
                        @NotNull final AbstractTerminalCommand command = clazz.getDeclaredConstructor().newInstance();
                        command.setEndPointLocator(this);
                        command.setSessionLocator(this);
                        command.setServiceLocator(this);
                        if (command instanceof HelpCommand) ((HelpCommand) command).setCommands(commands);
                        return command;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }).filter(Objects::nonNull).collect(Collectors.toMap(AbstractTerminalCommand::getName, command -> command)));
        return commands;
    }

}
