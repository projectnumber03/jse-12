package ru.shilov.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.service.ITerminalService;

public interface IServiceLocator {

    @NotNull
    ITerminalService getTerminalService();

}
