package ru.shilov.tm.command.task;

import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

public final class TaskRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskId = getEndPointLocator().getTaskEndPoint().getTaskId(session, getServiceLocator().getTerminalService().nextLine());
        getEndPointLocator().getTaskEndPoint().removeOneTaskByUserId(session, taskId);
        System.out.println("[ЗАДАЧА УДАЛЕНА]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление задачи";
    }

}
