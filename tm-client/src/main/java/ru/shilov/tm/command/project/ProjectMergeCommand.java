package ru.shilov.tm.command.project;

import com.google.common.base.Strings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.Project;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.Status;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;

public final class ProjectMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getEndPointLocator().getProjectEndPoint().getProjectId(session, getServiceLocator().getTerminalService().nextLine());
        @NotNull final Project p = getEndPointLocator().getProjectEndPoint().findOneProject(session, projectId);
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ПРОЕКТА:");
        p.setName(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        p.setDescription(getServiceLocator().getTerminalService().nextLine());
        try {
            @NotNull final DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            @NotNull final DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            p.setStart(outputFormatter.format(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), inputFormatter)));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            p.setFinish(outputFormatter.format(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), inputFormatter)));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        Arrays.asList(Status.values()).forEach(r -> System.out.println(String.format("%d. %s", r.ordinal() + 1, r.value())));
        System.out.println("ВЫБЕРИТЕ СТАТУС:");
        @NotNull
        String statusId = getServiceLocator().getTerminalService().nextLine();
        while (!statusCheck(statusId)) {
            System.out.println("ВЫБЕРИТЕ СТАТУС:");
            statusId = getServiceLocator().getTerminalService().nextLine();
        }
        p.setStatus(Status.values()[Integer.parseInt(statusId) - 1]);
        getEndPointLocator().getProjectEndPoint().mergeProject(session, p);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Редактирование проекта";
    }

    @NotNull
    private Boolean statusCheck(@Nullable final String statusId) {
        return !Strings.isNullOrEmpty(statusId)
                && statusId.matches("\\d+")
                && Integer.parseInt(statusId) <= Status.values().length;
    }

}
