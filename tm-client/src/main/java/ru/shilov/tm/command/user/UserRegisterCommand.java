package ru.shilov.tm.command.user;

import ru.shilov.tm.api.endpoint.Role;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.User;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;

public final class UserRegisterCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final User u = new User();
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(getServiceLocator().getTerminalService().nextLine());
        u.setRole(Role.USER);
        getEndPointLocator().getUserEndPoint().registerUser(u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "register";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Регистрация пользователя";
    }

}
