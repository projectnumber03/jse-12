package ru.shilov.tm.command.task;

import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TaskFindByNameOrDescriptionCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ФРАЗУ ДЛЯ ПОИСКА:");
        @NotNull final String searchPhrase = getServiceLocator().getTerminalService().nextLine();
        @NotNull final List<Task> tasks = getEndPointLocator().getTaskEndPoint().findTasksByNameOrDescription(session, searchPhrase);
        getServiceLocator().getTerminalService().printAllTasks(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск задач";
    }

}
