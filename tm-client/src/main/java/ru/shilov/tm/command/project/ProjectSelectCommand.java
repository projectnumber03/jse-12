package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.Project;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class ProjectSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getEndPointLocator().getProjectEndPoint().getProjectId(session, getServiceLocator().getTerminalService().nextLine());
        @NotNull final Project p = getEndPointLocator().getProjectEndPoint().findOneProject(session, projectId);
        getServiceLocator().getTerminalService().printProjectProperties(p);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Свойства проекта";
    }

}
