package ru.shilov.tm.command.task;

import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public final class TaskPersistCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        @NotNull final Task t = new Task();
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ЗАДАЧИ:");
        t.setName(getServiceLocator().getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        t.setDescription(getServiceLocator().getTerminalService().nextLine());
        try {
            @NotNull final DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            @NotNull final DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            t.setStart(outputFormatter.format(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), inputFormatter)));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            t.setFinish(outputFormatter.format(LocalDate.parse(getServiceLocator().getTerminalService().nextLine(), inputFormatter)));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        t.setUserId(session.getUserId());
        getEndPointLocator().getTaskEndPoint().persistTask(session, t);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создание задачи";
    }

}
