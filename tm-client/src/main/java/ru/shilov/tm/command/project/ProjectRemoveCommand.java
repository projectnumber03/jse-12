package ru.shilov.tm.command.project;

import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

public final class ProjectRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = getEndPointLocator().getProjectEndPoint().getProjectId(session, getServiceLocator().getTerminalService().nextLine());
        getEndPointLocator().getProjectEndPoint().removeOneProjectByUserId(session, projectId);
        System.out.println("[ПРОЕКТ УДАЛЕН]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление проекта";
    }

}
