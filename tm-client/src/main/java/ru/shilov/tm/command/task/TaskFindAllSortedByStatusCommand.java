package ru.shilov.tm.command.task;

import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.api.endpoint.Task;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskFindAllSortedByStatusCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final Session session = getSession();
        @NotNull final List<Task> tasks = getEndPointLocator().getTaskEndPoint().findTasksByUserId(session).stream()
                .sorted(Comparator.comparing(Task::getStatus))
                .collect(Collectors.toList());
        getServiceLocator().getTerminalService().printAllTasks(tasks);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список задач по статусу готовности";
    }

}
