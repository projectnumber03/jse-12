package ru.shilov.tm.error;

public class NoSuchSessionException extends RuntimeException {

    public NoSuchSessionException() {
        super("Сессия не найдена, выполните вход");
    }
}
